import { getLastLogLine } from "./docker.mjs";

import argsParser from "args-parser";
import child_process from "child_process";
import { Log } from "./log.mjs";

const args = argsParser(process.argv);

// Arguments
const containerArg = args["container"];
const onTimeoutArg = args["on-timeout"];
let INTERVAL = args["interval"] || 2 * 60;

if (!containerArg || typeof containerArg !== "string") {
  throw new Error("container argument not valid");
}

let observeContainerIds = containerArg.split(",");

if (!containerArg || !observeContainerIds) {
  Log.error("Invalid container name at $0");
  process.exit(1);
}

if (!onTimeoutArg) {
  Log.error("on-timeout argument must be defined");
  process.exit(1);
}

if (isNaN(INTERVAL)) {
  Log.error("Entered interval is not numeric type");
  process.exit(1);
} else {
  INTERVAL = Number(INTERVAL);
}

Log.info(`Started observing for: ${observeContainerIds.join(",")}`);
Log.info(`Check interval set to: ${INTERVAL} s`);

const handleOut = (err, data) => {
  if (err) {
    Log.error(data);
    process.exit(1);
  }
  if(data)
    Log.info(data);
};

let lastLines = new Map();

const check = async () => {
  Log.info("Performing check...");

  const containersToRemove = [];

  for await (const [containerId, lastLine] of lastLines) {
    const newLastLine = await getLastLogLine(containerId);

    if (lastLine === null) {
      lastLines.set(containerId, newLastLine);
      Log.info(`container ${containerId} running first time`);
      continue;
    }

    if (!newLastLine) {
      containersToRemove.push(containerId);
      Log.info(`could not found container ${containerId}`);
      continue;
    }

    if (newLastLine === lastLine) {
      Log.info(`no activity in ${containerId}, running timeout command`);
      
      const command = onTimeoutArg.replace(/{container}/g, containerId);
      child_process.execSync(command);
      handleOut(null, null);

      containersToRemove.push(containerId);
      continue;
    }

    lastLines.set(containerId, newLastLine);
  }


  // Delete from observations
  observeContainerIds = observeContainerIds.filter(
    (e) => !containersToRemove.includes(e)
  );

  // Delete from lines
  for (const containerId of containersToRemove) {
    lastLines.delete(containerId);
  }

  if(!lastLines.size) {
    clearTimeout(intervalHandle);
    Log.info(`no containers observed, exiting...`);
  }
};

for (const containerId of observeContainerIds) lastLines.set(containerId, null);

const intervalHandle = setInterval(check, INTERVAL * 1000);
