import { promisify } from "util";
import child_process from "child_process";

const execAsync = promisify(child_process.exec);

export const getLastLogLine = async (containerNameOrId) => {
  const { stdout, stderr } = await execAsync(
    `docker logs -t -n 1 ${containerNameOrId}`
  ).catch(e => e);
  const message = stdout || stderr;
  if (message.includes("No such container")) return null;
  return message;
};
