# NodeJS Script for Observing Docker Container Logs

## Overview

This NodeJS script monitors the logs of specified Docker containers. If a container's log shows no activity within a given interval, the script executes a specified command.

## Prerequisites

- NodeJS installed
- Docker installed and running

## Installation

1. Clone the repository or copy the script to your local environment.
2. Ensure you have the necessary dependencies by running:

    ```sh
    npm install args-parser
    ```

## Usage

The script is executed via the command line with specific arguments.

### Arguments

- `--container`: A comma-separated list of container IDs or names to monitor.
- `--on-timeout`: The command to execute when a container shows no log activity within the interval. Use `{container}` as a placeholder for the container ID.
- `--interval`: (Optional) The check interval in seconds. Defaults to 120 seconds.

### Example

```sh
node script.js --container container1,container2 --on-timeout "echo {container} has timed out" --interval 180
```

### Description of Example

- The script will monitor `container1` and `container2`.
- If no log activity is detected for 180 seconds, it will execute `echo {container} has timed out`, replacing `{container}` with the actual container ID.

## Script Explanation

1. **Argument Parsing**: The script uses `args-parser` to handle command-line arguments.
2. **Validation**: Checks if required arguments are provided and valid.
3. **Logging Setup**: Initializes logging to provide runtime information and error messages.
4. **Interval Setup**: Sets the interval for checking container logs.
5. **Log Monitoring**: Continuously monitors the last log line of each container.
6. **Timeout Handling**: Executes the specified command if no new log activity is detected for a container within the interval.

### Key Functions

- **getLastLogLine(containerId)**: Retrieves the last log line of the specified container.
- **check()**: Performs the log activity check and handles timeout scenarios.
- **handleOut(err, data)**: Handles the output or errors from executed commands.

## Error Handling

The script exits with an error message if:
- The `--container` argument is missing or invalid.
- The `--on-timeout` argument is missing.
- The `--interval` argument is not a valid number.

## Logging

The script uses a custom logging module (`log.mjs`) to provide informative messages about its operation, including:
- Start and stop messages
- Information about monitored containers
- Detected inactivity and executed commands

## Termination

The script will automatically terminate when there are no more containers to observe.

## License

This project is licensed under the MIT License.

## Contributions

Contributions are welcome! Please submit a pull request or open an issue to discuss improvements or fixes.

---

This README provides an overview and usage instructions for the script, ensuring users can set it up and run it effectively.